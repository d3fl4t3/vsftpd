FROM panubo/vsftpd:latest

COPY tail /bin
COPY tail /usr/bin
RUN chmod +x /bin/tail /usr/bin/tail

COPY entry.sh /
RUN chmod +x /entry.sh
